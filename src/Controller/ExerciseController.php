<?php

// Moar Templating
// 1. Dans le contrôleur ExerciseController rajouter une nouvelle méthode/route vers le chemin /exercise/second
// 2. Créer un nouveau template /templates/exercise/second.html.twig dans votre projet et faire le lien avec le contrôleur 
// (render et tout)
// 3. Dans la méthode du contrôleur, exposer au template un tableau tab avec 3-4 valeurs textuelles à l'intérieur
// 4. Dans le template, utiliser le tableau pour faire un for (utiliser l'auto complétion pour voir à quoi ça ressemble) 
// et faire en sorte de 
// répéter un paragraphe pour chaque tour de boucle avec la valeur actuelle de la boucle dans le paragrahpe
// 5. Afficher au dessus du for la taille du tableau en mode "there is x items"
// 6. Dans le template, faire en sorte que ça affiche les items du tableau sous forme de 
// ul > li plutôt que sous forme de paragraphe
// Le li devra se trouver dans le for et le for sera entouré par un ul
// 7. Rajouter une condition pour ne pas afficher le ul et la boucle si jamais le tableau ne contient aucun élément
// Il va falloir entourer le ul et le for d'un {% if %}

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



class ExerciseController extends AbstractController {

    /**
     * @Route("/exercise/first", name="exercise_first")
     */
    public function first() {
        return $this->render("exercise/first.html.twig", [
            "number" => random_int(1, 10)
        ]);
    }

    //  /**
    //  * @Route("/exercise/second")
    //  */
    // public function second() {
    //     return $this->render("exercise/second.html.twig", [
    //         "table" => ["un", "deux", "trois", "quatre", ""]
    //     ]);
    // }

    /**
     * @Route("/exercise/second", name="exercise_second")
     */
    public function second (){

        return $this->render("exercise/second.html.twig", [
            "tab" => ["Hello", "Bonjour", "Privet"]
        ]);
    }

}