<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class FormController extends AbstractController {

    /**
     * @Route("/form", name="form")
     */
    public function index(Request $request) {
        $valeur = $request->get("test");
        
        return $this->render("form.html.twig", [
            "valeur" => $valeur
        ]);
    }

}