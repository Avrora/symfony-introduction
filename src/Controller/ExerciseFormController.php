<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Dog;


class ExerciseFormController extends AbstractController {

    /**
     * @Route("/exercise/exo-form", name="dog_form")
     */
    public function formAction(Request $request) {
        $valeur1 = $request->get("id");
        $valeur2 = $request->get("name");
        $valeur3 = $request->get("breed");
        $valeur4 = $request->get("age");

        if ($valeur1 != null && $valeur2 != null && $valeur3 != null && $valeur4 != null) {
            $dog = new Dog($valeur1, $valeur2, $valeur3, $valeur4);

        } else {
            $dog = null;
        }
        
        return $this->render("exercise/exo-form.html.twig", [
            "dog" => $dog
        ]);
    }
}