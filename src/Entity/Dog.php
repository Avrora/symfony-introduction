<?php

namespace App\Entity;

class Dog {
    public $id;
    public $name;
    public $breed;
    public $age;

    public function __construct(int $id, string $name, string $breed, int $age) {
        $this->id = $id;
        $this->name = $name;
        $this->breed = $breed;
        $this->age = $age;
    }
}