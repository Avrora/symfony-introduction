# Symfony Introduction
Projet d'introduction du framework symfony en version 4.2.3 pour la promo 9 de Simplon Villeurbanne

## How To Use
1. Cloner le projet
2. Faire un `composer install` pour récupérer les dépendances
3. Faire un `bin/console server:run` pour lancer le serveur sur http://localhost:8000

## Exercices

Créer des routes et des templates
1. Créer un nouveau contrôleur ExerciseController
2. Rajouter une route /exercise/first qui pointera sur une méthode first du contrôleur Exercise (utiliser les annotations ou le yaml au choix)
3. Dans le dossier templates créer un sous dossier exercise et dedans rajouter un nouveau fichier first.html.twig
4. Faire en sorte que la méthode ExerciseController::first() utilise le template qu'on vient de créer
5. Dans le render, exposer au template une variable "number" qui aura comme valeur `random_int(1, 10)`. Afficher cette variable dans le template.
6. Dans le template faire en sorte d'afficher un paragraphe avec "You won" dedans seuleument si number est supérieur à 6

Moar Templating
1. Dans le contrôleur ExerciseController rajouter une nouvelle méthode/route vers le chemin /exercise/second
2. Créer un nouveau template /templates/exercise/second.html.twig dans votre projet et faire le lien avec le contrôleur (render et tout)
3. Dans la méthode du contrôleur, exposer au template un tableau `tab` avec 3-4 valeurs textuelles à l'intérieur
4. Dans le template, utiliser le tableau pour faire un for (utiliser l'auto complétion pour voir à quoi ça ressemble) et faire en sorte de répéter un paragraphe pour chaque tour de boucle avec la valeur actuelle de la boucle dans le paragrahpe
5. Afficher au dessus du for la taille du tableau en mode "there is x items"
6. Dans le template, faire en sorte que ça affiche les items du tableau sous forme de ul > li plutôt que sous forme de paragraphe
7. Rajouter une condition pour ne pas afficher le ul et la boucle si jamais le tableau ne contient aucun élément
